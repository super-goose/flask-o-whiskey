#print("hello")

from flask import Flask, render_template
app = Flask(__name__)

@app.route("/user/")
def hello():
    users = [ "frank", "steve", "alice", "bruce" ]
    return render_template('user.html', **locals())

@app.route("/")
def index():
    return "Hello World!"

if __name__ == "__main__":
    app.run()

# from flask import Flask, render_template
# app = Flask(__name__)

# @app.route("/<string:page_name>/")
# def render_static(page_name):
#     return render_template('%s.html' % page_name)

# def hello():
#     return "Hello World!"

# if __name__ == "__main__":
#     app.run()
