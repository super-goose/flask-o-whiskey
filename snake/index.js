const MAX_X = 30;//30;
const MAX_Y = 30;//30;
const CELLSIZE = 10;
const INTERVAL = 300;
const canvas = document.getElementById("field");
const field = canvas.getContext('2d');
let direction = 'd';
let snake = [ '2,0', '2,1']; //'5,5', '5,4', '5,3', '6,3', '7,3'];
let food = null;
let renderLoop = null;

function drawTheFood() {
    field.fillStyle = '#ff9100';
    const p = food.split(',');
    const x = Number(p[0]);
    const y = Number(p[1]);
    field.fillRect((x * CELLSIZE) + 1, (y * CELLSIZE) + 1, CELLSIZE - 2, CELLSIZE - 2);
}

function drawTheSnake() {
    field.fillStyle = '#fff';
    snake.forEach(segment => {
        const p = segment.split(',');
        const x = Number(p[0]);
        const y = Number(p[1]);
        field.fillRect((x * CELLSIZE) + 1, (y * CELLSIZE) + 1, CELLSIZE - 2, CELLSIZE - 2);
    });
}

function clearTheBoard() {
    field.clearRect(0, 0, MAX_X * CELLSIZE, MAX_Y * CELLSIZE);
}

function moveSnake(ateFood) {
    const newPoint = (() => {
        let head = snake[snake.length - 1].split(',');
        const x = Number(head[0]);
        const y = Number(head[1]);
        if (direction === 'd') {
            return `${x},${y + 1}`;
        }
        if (direction === 'u') {
            return `${x},${y - 1}`;
        }
        if (direction === 'l') {
            return `${x - 1},${y}`;
        }
        if (direction === 'r') {
            return `${x + 1},${y}`;
        }
    })();

    if (isCollision(snake, newPoint)) {
        clearInterval(renderLoop)
        throw new Error('collision');
    }

    snake.push(newPoint);

    if (newPoint === food) {
        food = null;
    } else {
        snake.splice(0, 1);
    }

    if (snake.length === MAX_Y * MAX_X) {
        clearInterval(renderLoop)
        throw new Error('victory!!')
    }    
}

function generateRandomFood() {
    let p;
    do {
        const x = Math.floor(Math.random() * MAX_X);
        const y = Math.floor(Math.random() * MAX_Y);
        p = `${x},${y}`
    } while (snake.includes(p));

    return p;
}

function isCollision(snake, point) {
    const p = point.split(',');
    const x = Number(p[0]);
    const y = Number(p[1]);
    if (x < 0 || x >= MAX_X || y < 0 || y >= MAX_Y) {
        return true;
    }
    return snake.includes(point);
}

// function Point(x, y) {
//     this.x = x;
//     this.y = y;
// }

document.onkeyup = function (evt) {
    const key = (evt || window.event).keyCode;

    if (key === 38) { direction = 'u'; } // up = 38
    if (key === 40) { direction = 'd'; } // down = 40
    if (key === 37) { direction = 'l'; } // left = 37
    if (key === 39) { direction = 'r'; } // right = 39
}

renderLoop = setInterval(() => {
    if (!food) {
        food = generateRandomFood();
    }
    moveSnake();
    clearTheBoard();
    drawTheSnake();
    drawTheFood();
}, INTERVAL)