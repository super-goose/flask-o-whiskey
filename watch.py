import sys
import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

if len(sys.argv) == 1:
    print("Which file will I be watchin', Yo??")
    sys.exit()

class MyHandler(FileSystemEventHandler):
    wait_for_it = True

    def on_modified(self, event):
        if event.src_path != ".\\{}".format(sys.argv[1]):
            return

        if self.wait_for_it:
            self.wait_for_it = False
        else:
            self.wait_for_it = True
            print("-------------{}-------------".format(sys.argv[1]))
            exec(open("skyrimmods.py").read())


event_handler = MyHandler()
observer = Observer()

observer.schedule(event_handler, path='.', recursive=False)
observer.start()

try:
    while True:
        time.sleep(1)
except KeyboardInterrupt:
    observer.stop()

observer.join()